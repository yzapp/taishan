#include "pch.h"
#include "comimpl.h"
#include "profileoperation.h"
#include "inc/wpptrace.h"
#include "comimpl.tmh"

using namespace YZ::Gaming::Com;

namespace
{
    unique_ptr<ProfileOperation> profileOp{ nullptr };
}

YZGAMINGCOM_API
DWORD
YZGamingCom_Initialize(
    PFN_PROFILEDEVICEEVENTHANDLER pfnProfileDeviceEventHandler,
    LPVOID                        lpContext
    )
{
    DWORD dwRetVal = ERROR_SUCCESS;
    
    if (profileOp = make_unique<ProfileOperation>(pfnProfileDeviceEventHandler, lpContext);
        bool{ profileOp })
    {
        if (!profileOp->Initialize())
        {
            dwRetVal = ERROR_OPEN_FAILED;
            WPPTrace(TRACE_LEVEL_ERROR, INIT, L"Failed in initializing the ProfileOperation object");

            profileOp.reset();
        }
        else
        {
            WPPTrace(TRACE_LEVEL_INFORMATION, INIT, L"The ProfileOperation object is initialized");
        }
    }
    else
    {
        dwRetVal = ERROR_CREATE_FAILED;
        WPPTrace(TRACE_LEVEL_ERROR, INIT, L"Failed in instantiating the ProfileOperation object");
    }

    return dwRetVal;
}

YZGAMINGCOM_API
DWORD
YZGamingCom_ReadProfileData(
    HPROFILEDEVICE hProfileDevice,
    LPPROFILEDATA lpProfileData
    )
{
    DWORD dwRetVal = ERROR_SUCCESS;

    if (hProfileDevice == nullptr && lpProfileData == nullptr)
    {
        dwRetVal = ERROR_INVALID_PARAMETER;
        WPPTrace(TRACE_LEVEL_ERROR, PROFILE, L"Either hProfileDevice is null or lpProfileData is null");
        return dwRetVal;
    }

    if (!bool{ profileOp })
    {
        dwRetVal = ERROR_RESOURCE_NOT_AVAILABLE;
        WPPTrace(TRACE_LEVEL_ERROR, PROFILE, L"The profile operation object is not initialized yet");
        return dwRetVal;
    }

    Profile profile{ 0 };
    DWORD fwVersion = 0;
    if (auto profileDevice{ reinterpret_cast<ProfileDevice*>(hProfileDevice) };
        profileDevice->ReadProfileData(0, profile, fwVersion))
    {
        lpProfileData->lightType = static_cast<PROFILEDATA::LIGHTTYPE>(profile.config_base_index.light_type);
        lpProfileData->lightRGB[0] = (profile.config_base_index.light_rgb & 0xff0000) >> 16; // red
        lpProfileData->lightRGB[1] = (profile.config_base_index.light_rgb & 0x00ff00) >> 8; // green
        lpProfileData->lightRGB[2] = (profile.config_base_index.light_rgb & 0x0000ff);      // blue
        lpProfileData->lightIntensity = profile.config_base_index.light_intensity;
        lpProfileData->motorIntensity = profile.config_base_index1.motor_intensity;
        lpProfileData->controllerMode = profile.config_base_index1.controller_mode;
        lpProfileData->nsPosition = profile.config_base_index1.ns_position;
        lpProfileData->turboA = profile.config_turbo.A_turbo;
        lpProfileData->turboB = profile.config_turbo.B_turbo;
        lpProfileData->turboX = profile.config_turbo.X_turbo;
        lpProfileData->turboY = profile.config_turbo.Y_turbo;
        lpProfileData->turboL1 = profile.config_turbo.L1_turbo;
        lpProfileData->turboR1 = profile.config_turbo.R1_turbo;
        lpProfileData->turboOpen = profile.config_turbo.turbo_open;
        lpProfileData->lStickDeadZoneValue = profile.config_stick_trigger.Lstick_deadzone_value;
        lpProfileData->rStickDeadZoneValue = profile.config_stick_trigger.Rstick_deadzone_value;
        lpProfileData->lStickSensitivity = profile.config_stick_trigger.left_stick_sensitivity;
        lpProfileData->rStickSensitivity = profile.config_stick_trigger.right_stick_sensitivity;
        lpProfileData->lTriggerSensitivity = profile.config_stick_trigger.left_trigger_sensitivity;
        lpProfileData->rTriggerSensitivity = profile.config_stick_trigger.right_trigger_sensitivity;
        lpProfileData->stickCalibration = profile.config_stick_trigger.enter_stick_cal;
        lpProfileData->stickCalibrationSucess = profile.config_stick_trigger.stick_cal_sucess;
        lpProfileData->triggerCalibration = profile.config_stick_trigger.enter_trigger_cal;
        lpProfileData->triggerCalibrationSucess = profile.config_stick_trigger.trigger_cal_sucess;
        for (int i = 0; i < KEYCODE::MAX_KEY; i++)
        {
            lpProfileData->keys[i] = static_cast<KEYCODE>(profile.mk_mode_keydata[i].mapping);
        }
        lpProfileData->fwVersion = fwVersion;

        WPPTrace(TRACE_LEVEL_INFORMATION, PROFILE, L"Succeeded in reading profile data");
    }
    else
    {
        dwRetVal = ERROR_READ_FAULT;
        WPPTrace(TRACE_LEVEL_ERROR, PROFILE, L"Read profile data failed");
    }

    return dwRetVal;
}

DWORD YZGamingCom_WriteProfileData(
    HPROFILEDEVICE hProfileDevice,
    LPPROFILEDATA lpProfileData
    )
{
    DWORD dwRetVal = ERROR_SUCCESS;

    if (hProfileDevice == nullptr && lpProfileData == nullptr)
    {
        dwRetVal = ERROR_INVALID_PARAMETER;
        WPPTrace(TRACE_LEVEL_ERROR, INIT, L"Either hProfileDevice is null or lpProfileData is null");
        return dwRetVal;
    }

    if (!bool{ profileOp })
    {
        dwRetVal = ERROR_RESOURCE_NOT_AVAILABLE;
        WPPTrace(TRACE_LEVEL_ERROR, INIT, L"The profile operation object is not initialized yet");
        return dwRetVal;
    }

    Profile profile{ 0 };
    profile.config_base_index.light_type = lpProfileData->lightType;
    profile.config_base_index.light_rgb  = lpProfileData->lightRGB[0] << 16; // red
    profile.config_base_index.light_rgb |= lpProfileData->lightRGB[1] << 8;  // green
    profile.config_base_index.light_rgb |= lpProfileData->lightRGB[2];       // blue
    profile.config_base_index.light_intensity = lpProfileData->lightIntensity;
    profile.config_base_index1.motor_intensity = lpProfileData->motorIntensity;
    profile.config_base_index1.controller_mode = lpProfileData->controllerMode;
    profile.config_base_index1.ns_position = lpProfileData->nsPosition;
    profile.config_turbo.A_turbo = lpProfileData->turboA;
    profile.config_turbo.B_turbo = lpProfileData->turboB;
    profile.config_turbo.X_turbo = lpProfileData->turboX;
    profile.config_turbo.Y_turbo = lpProfileData->turboY;
    profile.config_turbo.L1_turbo = lpProfileData->turboL1;
    profile.config_turbo.R1_turbo = lpProfileData->turboR1;
    profile.config_turbo.turbo_open = lpProfileData->turboOpen;
    profile.config_stick_trigger.Lstick_deadzone_value = lpProfileData->lStickDeadZoneValue;
    profile.config_stick_trigger.Rstick_deadzone_value = lpProfileData->rStickDeadZoneValue;
    profile.config_stick_trigger.left_stick_sensitivity = lpProfileData->lStickSensitivity;
    profile.config_stick_trigger.right_stick_sensitivity = lpProfileData->rStickSensitivity;
    profile.config_stick_trigger.left_trigger_sensitivity = lpProfileData->lTriggerSensitivity;
    profile.config_stick_trigger.right_trigger_sensitivity = lpProfileData->rTriggerSensitivity;
    profile.config_stick_trigger.enter_stick_cal = lpProfileData->stickCalibration;
    profile.config_stick_trigger.stick_cal_sucess = lpProfileData->stickCalibrationSucess;
    profile.config_stick_trigger.enter_trigger_cal = lpProfileData->triggerCalibration;
    profile.config_stick_trigger.trigger_cal_sucess = lpProfileData->triggerCalibrationSucess;
    for (int i = 0; i < MAP_KEY_MAX; i++)
    {
        profile.mk_mode_keydata[i].mapping = lpProfileData->keys[i];
    }

    if (auto profileDevice{ reinterpret_cast<ProfileDevice*>(hProfileDevice) };
        profileDevice->WriteProfileData(0, profile))
    {
        WPPTrace(TRACE_LEVEL_INFORMATION, PROFILE, L"Succeeded in writing profile data");
    }
    else
    {
        dwRetVal = ERROR_WRITE_FAULT;
        WPPTrace(TRACE_LEVEL_ERROR, INIT, L"Write profile data failed");
    }

    return dwRetVal;
}

YZGAMINGCOM_API
VOID
YZGamingCom_Uninitialize(
    VOID
    )
{
    if (bool{ profileOp })
    {
        profileOp->Uninitialize();
        profileOp.reset(nullptr);

        WPPTrace(TRACE_LEVEL_INFORMATION, INIT, L"The ProfileOperation object is uninitialized");
    }
}
