﻿#pragma once
#include <windows.h>

namespace YZ::Gaming::Com
{
    constexpr auto REPORTID_1 = 0x01;
    constexpr auto REPORTID_2 = 0x02;
    constexpr auto REPORTID_3 = 0x03;

    constexpr auto PROFILENUMBER = 24;
    constexpr auto MAP_KEY_MAX = 20;

    //0->select物理键
    //1->L2物理键
    //2->L1物理键
    //3->L3D_UP物理键
    //4->L3D_DOWN物理键
    //5->L3D_LEFT物理键
    //6->L3D_RIGHT物理键
    //7->L3物理键
    //8->DPAD_UP物理键
    //9->DPAD-DOWN物理键
    //10->DPAD-LEFT物理键
    //11->DPAD-RIGHT物理键
    //12->start物理键
    //13->R2物理键
    //14->R1物理键
    //15->A物理键
    //16->B物理键
    //17->X物理键
    //18->Y物理键
    //19->R3物理键
    typedef union
    {
        UINT32 data[PROFILENUMBER];      //PROFILENUMBER: 24
        struct
        {
            struct   //ensure 32bit(4 bytes) every struct
            {
                UINT32 light_type : 3;  //0:单色常亮，1:单色呼吸，2:RGB呼吸，3:RGB循环，4:RGB波浪
                UINT32 light_rgb : 24;  //R:8 G:8 B:8
                UINT32 light_intensity : 4;  //0:关闭，1~5档
                UINT32 reserve : 1;
            } config_base_index;

            struct   //ensure 32bit(4 bytes) every struct
            {
                UINT32 motor_intensity : 3;  //0:关闭，1:弱，2:强
                UINT32 controller_mode : 1;  //0:手柄，1键鼠
                UINT32 ns_position : 1;  //0:XBOX按键布局 1：NS按键布局
                UINT32 reserve : 27;
            } config_base_index1;

            struct   //ensure 32bit(4 bytes) every struct
            {
                UINT32 A_turbo : 1;  //0:关闭   1:开启
                UINT32 B_turbo : 1;  //0:关闭   1:开启
                UINT32 X_turbo : 1;  //0:关闭   1:开启
                UINT32 Y_turbo : 1;  //0:关闭   1:开启
                UINT32 L1_turbo : 1;  //0:关闭   1:开启
                UINT32 R1_turbo : 1;  //0:关闭   1:开启
                UINT32 turbo_open : 1;  //0:关闭   1:开启
                UINT32 reserve : 25;
            } config_turbo;

            struct   //ensure 32bit(4 bytes) every struct
            {
                UINT32  Lstick_deadzone_value : 8;   //0~20 （中心死区%）
                UINT32  Rstick_deadzone_value : 8;   //0~20 （中心死区%）
                UINT32  left_stick_sensitivity : 3;    //0~5
                UINT32  right_stick_sensitivity : 3;    //0~5
                UINT32  left_trigger_sensitivity : 3;   //0~5
                UINT32  right_trigger_sensitivity : 3;  //0~5
                UINT32  enter_stick_cal : 1;  //0:退出摇杆校准 1:进入摇杆校准
                UINT32  stick_cal_sucess : 1;  //0:摇杆校准失败 1:摇杆校准成功
                UINT32  enter_trigger_cal : 1;  //0:退出扳机校准 1:进入扳机校准
                UINT32  trigger_cal_sucess : 1;  //0:扳机校准失败 1:扳机校准成功 
            } config_stick_trigger;

            struct
            {
                UINT32 mapping : 8;  //按键映射键码
                UINT32 reserve : 24;
            } mk_mode_keydata[MAP_KEY_MAX];  //MAP_KEY_MAX->20
        };
    } Profile;

    enum class ProfileCmd : BYTE
    {
        R_INFO = 0x10,			//预读Profile命令，下位机应答相关信息，版本号等
        R_PF_DATA = 0x11,		//读完整Profile数据
        R_V_DATA = 0x12,		//校验读取的完整Profile数据
        W_INFO = 0x20,			//预读写Profile命令，下位机应答相关信息，版本号等
        W_PF_DATA = 0x21,		//写完整Profile数据
        W_V_DATA = 0x22,		//校验写完整Profile数据
        W_S_DATA = 0x23     	//保存写完整Profile数据（通知下位机保存到Flash中）
    };
}
