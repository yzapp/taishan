﻿using CommunityToolkit.Mvvm.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using yz.gaming.accessoryapp.Controls;
using yz.gaming.accessoryapp.Model;
using yz.gaming.accessoryapp.ViewModel;
using yz.gaming.accessoryapp.ViewModel.ControllerPage;

namespace yz.gaming.accessoryapp.View.ControllerPage
{
    /// <summary>
    /// KeyPageView.xaml 的交互逻辑
    /// </summary>
    public partial class KeyPageView : Page, IPageViewInterface
    {
        KeyPageViewModel _viewModel = null;
        public IViewModel ViewModel => _viewModel;

        public KeyPageView()
        {
            InitializeComponent();

            _viewModel = Ioc.Default.GetRequiredService<KeyPageViewModel>();
            DataContext = _viewModel;

            _viewModel.ChildPageMap = new Dictionary<IPageListItem, IPageViewInterface>
            {
                { KeyDescription, new KeyDescriptionPageView() },
                { ControllerMode, null },
                { UseNintendoLayout, null },
                { RepeatMode, new RepetPageView() },
                { CalibrationAndAdvancedSettings, new CalibrationAndAdvancedSettingPageView() }
            };

            KeyDescription.OnSelectedStateChange += _viewModel.OnButtonSelectedStateChange;
            ControllerMode.OnSelectedStateChange += _viewModel.OnButtonSelectedStateChange;
            UseNintendoLayout.OnSelectedStateChange += _viewModel.OnButtonSelectedStateChange;
            RepeatMode.OnSelectedStateChange += _viewModel.OnButtonSelectedStateChange;
            CalibrationAndAdvancedSettings.OnSelectedStateChange += _viewModel.OnButtonSelectedStateChange;

            KeyDescription.OnHovedStateChange += _viewModel.OnButtonHovedStateChange;
            ControllerMode.OnHovedStateChange += _viewModel.OnButtonHovedStateChange;
            UseNintendoLayout.OnHovedStateChange += _viewModel.OnButtonHovedStateChange;
            RepeatMode.OnHovedStateChange += _viewModel.OnButtonHovedStateChange;
            CalibrationAndAdvancedSettings.OnHovedStateChange += _viewModel.OnButtonHovedStateChange;

            KeyDescription.OnClick += _viewModel.OnButtonClick;
            ControllerMode.OnClick += _viewModel.OnButtonClick;
            UseNintendoLayout.OnClick += _viewModel.OnButtonClick;
            RepeatMode.OnClick += _viewModel.OnButtonClick;
            CalibrationAndAdvancedSettings.OnClick += _viewModel.OnButtonClick;

            this.Loaded += KeyPage_Loaded;
        }

        public IPageViewInterface Init(INavigationSupport navigationParent)
        {
            _viewModel.OnChildPageNavigation += navigationParent.ChildPageNavgation;
            _viewModel.Initialization();

            foreach (var item in _viewModel.ChildPageMap)
            {
                item.Value?.Init(navigationParent);
            }

            KeyDescription.IsHoved = true;
            ControllerMode.LeftElement = (byte)0;
            ControllerMode.RightElement = (byte)1;
            _viewModel.Model.OnProfileReresh += OnProfileReresh;

            return this;
        }

        private void KeyPage_Loaded(object sender, RoutedEventArgs e)
        {
            _viewModel.UnSelectAllItem();
            _viewModel.Initialization();

            if (_viewModel.HovedItem != null)
            {
                _viewModel.HovedItem.IsHoved = true;
                _viewModel.HovedItem.IsSelected = true;
            }
            
            ControllerMode.SelectElement = _viewModel.ControllerMode;
            UseNintendoLayout.IsChecked = _viewModel.UseNintendoLayout;
        }

        private void UseNintendoLayout_OnCheckedStateChanged(IPageListItem sender, bool isChecked)
        {
            _viewModel.UseNintendoLayout = isChecked;
        }

        private void ControllerMode_OnSelectedElementChanged(IPageListItem sender, object value)
        {
            _viewModel.ControllerMode = (byte)value;
        }

        private void OnProfileReresh(YzProfileModel model)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                //System.Diagnostics.Debug.WriteLine($"Trace : OnProfileReresh [ControllerMode:{model.ControllerMode}] [KeyLayout:{model.KeyLayout}]");
                _viewModel.ControllerMode = model.ControllerMode;
                _viewModel.UseNintendoLayout = model.KeyLayout == 1;

                ControllerMode.SelectElement = _viewModel.ControllerMode;
                UseNintendoLayout.IsChecked = _viewModel.UseNintendoLayout;
            }));
        }
    }
}
