﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace yz.gaming.accessoryapp.Api
{
    public class YzCommonApi
    {
        public const int SUCCESS = 0;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void Pfn_ProfileDeviceEventHandler(
            ProfileDeviceEventEnmu deviceEvent,
            IntPtr hProfileDevice,
            UIntPtr wParam,
            IntPtr lParam,
            IntPtr lpContext);

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void Pfn_DeviceUpdateValueCallBack(int value);

        const string DLL_PATH = @"Api/yz.gaming.com.dll";

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingCom_Initialize(
            Pfn_ProfileDeviceEventHandler pfnProfileDeviceEventHandler,
            IntPtr lpContext);

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingCom_ReadProfileData(
            IntPtr hProfileDevice,
            IntPtr lpProfileData);

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingCom_WriteProfileData(
            IntPtr hProfileDevice,
            IntPtr lpProfileData);

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void YZGamingCom_Uninitialize();

        #region WIM

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingWmi_Initialize();

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YzGamingWmi_Uninitialize();

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YzGamingWmi_SetPowerMode(byte modeValue);

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YzGamingWmi_GetPowerMode(ref int piModeValue);
        #endregion

        #region FwUpdate
        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int YZGamingUpdate_Initialize(
            [MarshalAs(UnmanagedType.LPStr)] String filePath,
            IntPtr caller,
            Pfn_DeviceUpdateValueCallBack pfnUpdateProgress,
            Pfn_DeviceUpdateValueCallBack pfnUpdateStatus);

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void YZGamingUpdate_Uninitialize();

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingUpdate_GetDeviceVersionInUpdatePackage();

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingUpdate_Start();

        [DllImport(DLL_PATH, CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int YZGamingUpdate_Stop();
        #endregion

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct YZDpadStruct
        {
            public LightTypeEnum LightType;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public byte[] LightRGB;        //0:red, 1:green, 2:blue
            public byte LightIntensity;    //0:关闭, 1~5档

            public byte MotorIntensity;    //0:关闭, 1:弱, 2:强
            public byte ControllerMode;    //0:手柄, 1:键鼠
            public byte NsPosition;        //0:XBOX按键布局, 1:NS按键布局

            public byte TurboA;            //0:关闭, 1:开启
            public byte TurboB;            //0:关闭, 1:开启
            public byte TurboX;            //0:关闭, 1:开启
            public byte TurboY;            //0:关闭, 1:开启
            public byte TurboL1;           //0:关闭, 1:开启
            public byte TurboR1;           //0:关闭, 1:开启
            public byte TurboOpen;         //0:关闭, 1:开启

            public byte LStickHeadZoomValue;       //0~20（中心死区%）
            public byte RStickHeadZoomValue;       //0~20（中心死区%）
            public byte LStickSensitivity;         //0~5
            public byte RStickSensitivity;         //0~5
            public byte LTriggerSensitivity;       //0~5
            public byte RTriggerSensitivity;       //0~5
            public byte StickCalibration;          //0:退出摇杆校准, 1:进入摇杆校准
            public byte StickCalibrationSucess;    //0:摇杆校准失败, 1:摇杆校准成功
            public byte TriggerCalibration;        //0:退出扳机校准, 1:进入扳机校准
            public byte TriggerCalibrationSucess;  //0:扳机校准失败, 1:扳机校准成功

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = (int)KeyCodeEnum.MAX_KEY)]
            public byte[] MkModeKeyData;
            public uint FwVersion;
        }

        public enum LightTypeEnum : uint
        { 
            MonoBright,
            MonoBreath,
            RGBBreath,
            RGBCycle,
            RGBWave
        }

        public enum KeyCodeEnum : byte
        {
            Back = 0x00,
            L2,
            L1,
            L3D_UP,
            L3D_DOWN,
            L3D_LEFT,
            L3D_RIGHT,
            L3,
            DPAD_UP,
            DPAD_DOWN,
            DPAD_LEFT,
            DPAD_RIGHT,
            Start,
            R2,
            R1,
            A,
            B,
            X,
            Y,
            R3,
            Quick,
            MAX_KEY
        }

        /// <summary>
        /// The profile device events.
        /// </summary>
        public enum ProfileDeviceEventEnmu
        {
            DeviceAdded,
            DeviceRemoved,
            KeyPressed,
            TriggerPressed,
            ThumbPressed,
            ModeChanged,
            VersionArrived
        }

        public enum  TriggerKeyEnmu { Left, Right }

        public enum ThumbEnmu { LX, LY, RX, RY }

        public enum ThumbKeyEnmu { Left, Right }

        public enum ThumbDirectionEnmu { UP, DOWN, LEFT, RIGHT }

        public enum KeyEventTypeEnmu { UP, DOWN }

        public enum KeyPressTypeEnmu { ShortPress, LongPress, Ignore }
    }
}
