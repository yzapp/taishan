﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using yz.gaming.accessoryapp.ViewModel.Main;
using static yz.gaming.accessoryapp.Api.YzCommonApi;

namespace yz.gaming.accessoryapp.ViewModel.ControllerPage
{
    public class KeyDescriptionPageViewModel : ViewModelBase, ITipButtomMapSupport, IMenuSupport
    {
        public event Action<Grid> OnSelectedMenuChanged;

        public List<bool> TipButtomMap { get; } = new List<bool> {true, false, true, true, true, false, false, false };

        public List<Grid> MenuList { get; set; }

        public Grid CurrentMenu { get; set; }

        public int CurrentIndex { get; set; } = 0;

        public KeyDescriptionPageViewModel()
            : base()
        {
        }

        public override void Initialization()
        {
            Title = GetString("KeyDescription");
        }

        public void MenuSelect(int index)
        {
            if (index >= 0 && index < MenuList.Count)
            {
                OnSelectedMenuChanged?.Invoke(MenuList[index]);
            }
        }

        public override void HandleKeyEvent(KeyCodeEnum key, KeyPressTypeEnmu type)
        {
            switch (key)
            {
                case KeyCodeEnum.L1:
                    if (CurrentIndex > 0)
                    {
                        OnSelectedMenuChanged?.Invoke(MenuList[CurrentIndex - 1]);
                    }
                    break;
                case KeyCodeEnum.R1:
                    if (CurrentIndex < MenuList.Count -1)
                    {
                        OnSelectedMenuChanged?.Invoke(MenuList[CurrentIndex + 1]);
                    }
                    break;
                default:
                    base.HandleKeyEvent(key, type);
                    break;
            }
        }
    }
}
